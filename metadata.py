#!/usr/bin/env python

import re
import sys
from astropy.io import fits
import numpy as np
import os

for filename in sys.argv[1:]:
  f = fits.open(filename)
  head = f[0].header['HISTORY']
  if 'PROJID' in f[0].header:
    print("{} already has the additional metadata".format(filename))
  else:
    for item in head:
      m = re.search("20[0-9]{2}\.[0-9]\.[0-9]{5}", item)
      if m is not None:
        projid = m.group(0)
        break;

    f[0].header['PROJID']=projid
    os.system("rm -f {}".format(filename))
    f.writeto(filename)
